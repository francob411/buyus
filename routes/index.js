
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Express' });
};

exports.details = function(req, res){
  res.render('details', { title: 'Details', id: req.params.id });
};

exports.buy = function(req, res){
  
  var data = {};
  res.header('Content-Type' , 'application/json' );

  res.send(JSON.stringify(data));

};